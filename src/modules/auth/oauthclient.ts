import { OAuthClientModel } from '../../db/models/oauthClient.model';

import { IPersistableEntity, PersistableEntity } from '../common';

export interface IOAuthClient extends IPersistableEntity {
    clientId: string;
    clientSecret: string;
    redirectUri: string;
    responseType: string;
    state: string;
}

export class Oauthclient extends PersistableEntity implements IOAuthClient {
    private _clientId: string;
    private _clientSecret: string;
    private _redirectUri: string;
    private _responseType: string;
    private _state: string;

    get clientId(): string {
        return this._clientId;
    }

    setClientId(value): Oauthclient {
        this._clientId = value;
        return this;
    }

    get clientSecret(): string {
        return this._clientSecret;
    }

    setClientSecret(value): Oauthclient {
        this._clientSecret = value;
        return this;
    }

    get redirectUri(): string {
        return this._redirectUri;
    }

    setRedirectUri(value): Oauthclient {
        this._redirectUri = value;
        return this;
    }

    get responseType(): string {
        return this._responseType;
    }

    setResponseType(value): Oauthclient {
        this._responseType = value;
        return this;
    }

    get state(): string {
        return this._state;
    }

    setState(value): Oauthclient {
        this._state = value;
        return this;
    }

    toJSON() {
        const base = super.toJSON();

        return {
            ...base,
            clientId: this._clientId,
            clientSecret: this._clientSecret,
            redirectUri: this._redirectUri,
            responseType: this._responseType,
            state: this._state,
        };
    }

    toModelValues() {
        const raw = this.toJSON();

        for (const key of Object.keys(raw)) {
            if (!raw[key]) delete raw[key];
        }
        return raw;
    }

    static fromRaw(
        raw: KeyValueHash | typeof OAuthClientModel | any,
        options?: KeyValueHash,
    ): Oauthclient {
        const instance = new this();

        instance
            .setId(raw.id)
            .setName(raw.name)
            .setCreatedAt(raw.createdAt)
            .setUpdatedAt(raw.updatedAt)
            .setClientId(raw.clientId)
            .setClientSecret(raw.clientSecret)
            .setRedirectUri(raw.redirectUri)
            .setResponseType(raw.responseType)
            .setState(raw.state)

        return instance;
    }
}
