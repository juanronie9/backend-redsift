import mongoose, { Document, Schema } from 'mongoose';
import { baseEntitySchema } from './base-entity-schema';

export interface IAccessTokenModel extends Document {
    _id: string;
    name: string;
    token: string;
    oauthClientId: string;
    userId: string;
    scope: string;
    createdAt: Date;
    updatedAt: Date;
}

export const accessTokenSchema = new mongoose.Schema(
    {
        ...baseEntitySchema,
        token: String,
        oauthClientId: { type: Schema.Types.ObjectId, ref: 'OAuthClient' },
        userId: { type: Schema.Types.ObjectId, ref: 'User' },
        scope: String,
    },
    { timestamps: true },
);

export const AccessTokenModel = mongoose.model<IAccessTokenModel>('AccessToken', accessTokenSchema);
