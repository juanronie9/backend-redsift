# WRITTEN TASK

## Approach to completing this task

### Requirements:

-   TypeScript
-   ExpressJS
-   MongoDB
-   Queue Service. Example: AWS SQS
-   Testing: Jest

### Flow:

1. Create API that receives an array of user emails.
    ```
    POST /api/v1/installations
    { 
        "users": ["test1@test1.test1", "test2@test2.test2"] 
    }
    ```
    1. Validate emails.
    2. Check in the database (installations collection) which users exists and has performed the installation previously.
        - For users that already exist and completed the installation, create array response.
        - For users that didn't exist and didn't complete the installation, create array response. For each of the user:
            - Insert new row in DB. Example:  `email: test@test.test, installation: 0`
            - Call to Queue `Producer` function. Producer: insert events in a queue. Example: `WorkerQueue`.   
    3. Return API information.
   ```
   {
      "installation_completed": ["test1@test1.test1"],
      "installation_pending": ["test2@test2.test2"] 
   }
    ```
    

2. `Consumer handler`: consume events from the queue `WorkerQueue`:
    1. Read data received in the event and parse properly.
    2. Call to external API, installOnDMARCProduct.
        - If success response (HTTP 200 user): Update row in DB. Example: `email: test@test.test, installation: 1`
        - If error response: create new event into another queue, `WorkerDlq` (where all failed events are). These failed items will retry to be consumed in several attempts later.
    



### Split into small tasks
In order to develop this flow, the following tasks can be created:

- API, Router and Controller
- API Security. Example: JWT, OAuth
- Input data validators
- Gateway/Repository to perform queries in DB
- Service Worker, with Producer and Consumer functions 
- Handler Consumer waiting for consuming events
- Service with requests to external API. installOnDMARCProduct
- Unit test 
- E2E test


### How you would test it
- **Unit Testing** for each of the individual units or components. The purpose is to validate that each unit of the software code performs as expected.
- **E2E Testing** APIs, the entire software product from beginning to end to ensure the application flow behaves as expected.


### What edges you would have to consider and how you would solve them:

- The installation process is a bit unstable, so sometimes it won't work on the first attempt.
  
    **SOLUTION**: There is a queue for that `WorkerDlq` (where all failed events are). These failed items will retry to be consumed in several attempts later.
    

- You might receive requests for installs in parallel, or close together
    
    **SOLUTION**: The api can receive many requests at once. Then it is important the solution provided, using the queuing service, example AWS SQS, there is no problem because events are generated.   


- How to handle sudden unexpected downtime of the service you're building or the service that you depend on.
    
    **SOLUTION**:

    - High availability IT systems (99.99%) should be used as standard to ensure the preservation of data and applications. Example: AWS.
    - If the api is down, it cannot receive any requests.
    - If any of the service you depend on is down:
        - return an error in the api. Example: HTTP_CODE 503 service unavailable
        - log the error in the application and review 
        - use healthcheck from your services  
        - try to create and save automated backups of your DB instance securely for a user-specified retention period
        - monitoring infrastructures and applications. Example: AWS CloudWatch
        - try to use elastic load balancing, autoescaling to automatically adjust resources as needed and distribute network traffic to improve the scalability of applications.

- How might the service calling the API know when a given installation request was completed
    
    **SOLUTION**: There is a list of completed installations in DB. An api could be created returning this information. Another option is to send a notification.
