import { ExpressController, ExpressJSONSearchPresenter } from '../../modules/core';

import { UserGateway } from '../../modules/users/user-gateway';
import { GetUsersInteractor, GetUsersRequestObject, validationSchema as getAllSchema } from '../../modules/users/get-users-interactor';
import { AuthMiddleware } from '../../modules/core/auth.middleware';

export class UsersController extends ExpressController {
    constructor() {
        super();

        this.router.get('/', AuthMiddleware, this.validator.validateQuery(getAllSchema), this.getUsers.bind(this));
    }

    async getUsers(req, res, next) {
        const userGateway = new UserGateway();
        const interactor = new GetUsersInteractor();

        const presenter = new ExpressJSONSearchPresenter(req, res, next);

        const request = req.query as GetUsersRequestObject;

        interactor.setPresenter(presenter).setUserGateway(userGateway);
        await interactor.execute(request);
    }
}
