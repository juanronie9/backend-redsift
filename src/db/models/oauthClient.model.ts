import mongoose, { Document } from 'mongoose';
import { baseEntitySchema } from './base-entity-schema';

export interface IOAuthClientModel extends Document {
    _id: string;
    name: string;
    clientId: string;
    clientSecret: string;
    redirectUri: string,
    responseType: string,
    createdAt: Date;
    updatedAt: Date;
}

export const oauthClientSchema = new mongoose.Schema(
    {
        ...baseEntitySchema,
        clientId: String,
        clientSecret: String,
        redirectUri: String,
        responseType: String,
    },
    { timestamps: true },
);

export const OAuthClientModel = mongoose.model<IOAuthClientModel>('OAuthClient', oauthClientSchema);
