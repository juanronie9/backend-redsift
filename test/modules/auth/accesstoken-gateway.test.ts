import mongoose from 'mongoose';

import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import { AccessTokenGateway } from '../../../src/modules/auth/accesstoken-gateway';
import { AccessToken } from '../../../src/modules/auth/accesstoken';
import { AccessTokenModel } from '../../../src/db/models/accessToken.model';

describe('AccessTokenGateway', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();
    });
    afterAll(async () => {
        // await db.close()
    });

    describe('createAuthToken', () => {
        it('should create auth token', async () => {
            const accessTokenGateway = new AccessTokenGateway();

            const accessToken = new AccessToken();
            const oauthClientIdBefore = new mongoose.Types.ObjectId();
            accessToken.setOAuthClientId(oauthClientIdBefore);

            const response = await accessTokenGateway.createAccessToken(accessToken);
            expect(response).not.toBeNull();
            expect(response.token).not.toBeNull();
            expect(response.oauthClientId).toBe(oauthClientIdBefore);

            const accessTokenExists = await AccessTokenModel.findById(response.id).exec();
            expect(accessTokenExists).toBeDefined();
        });
    });
});