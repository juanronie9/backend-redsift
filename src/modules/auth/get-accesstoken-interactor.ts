import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { AuthService } from './auth-service';
import { OAuthClientGateway } from './oauthclient-gateway';
import { AccessTokenGateway } from './accesstoken-gateway';
import { AccessToken } from './accesstoken';

export interface GetAccessTokenRequestObject extends IRequestObject {
    grantType: string,
    clientId: string,
    clientSecret: string;
    email: string;
    password: string;
}

export const validationSchema = joi.object().keys({
    grantType: joi
        .string()
        .required(),
    clientId: joi
        .string()
        .required(),
    clientSecret: joi
        .string()
        .required(),
    email: joi
        .string()
        .required(),
    password: joi
        .string()
        .required(),
});


export class GetAccesstokenInteractor extends Interactor {

    private _oauthClientGateway: OAuthClientGateway = null;
    private _accessTokenGateway: AccessTokenGateway = null;

    setOAuthClientGateway(gateway: OAuthClientGateway) {
        this._oauthClientGateway = gateway;
        return this;
    }

    setAccessTokenGateway(gateway: AccessTokenGateway) {
        this._accessTokenGateway = gateway;
        return this;
    }

    async execute(request: GetAccessTokenRequestObject) {
        try {
            const data = await this._oauthClientGateway.validateOAuthClient({
                clientId: request.clientId,
                clientSecret: request.clientSecret,
                redirectUri: null
            })
            if (!data) {
                return this.presenter.error([ 'OAuth client not found' ]);
            }

            const authService = new AuthService();
            const user = await authService.login(request.email, request.password);
            if (!user) {
                return this.presenter.error([ 'User not found' ]);
            }

            const accessToken = new AccessToken();
            accessToken.setOAuthClientId(data._id);
            accessToken.setUserId(user._id);
            const { token } = await this._accessTokenGateway.createAccessToken(accessToken);

            const result = {
                access_token: token,
                expires_in: 3600,
            }
            return this.presenter.success(result);
        } catch (e) {
            logger.error('Failed to get access token', request, e);
            return this.presenter.error([e.message]);
        }
    }
}
