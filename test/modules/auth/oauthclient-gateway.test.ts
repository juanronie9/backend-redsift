import faker from 'faker';
import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import { OAuthClientGateway } from '../../../src/modules/auth/oauthclient-gateway';
import { OAuthClientModel } from '../../../src/db/models/oauthClient.model';

describe('OAuthClientGatway', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();
    })

    afterAll(async () => {
        // await db.close()
    })

    describe('validateOAuthClient', () => {
        it('should not return data: not found', async () => {
            const oauthClientGateway = new OAuthClientGateway();

            const authTokenResponse = await oauthClientGateway.validateOAuthClient({
                clientId: faker.datatype.uuid(),
                clientSecret: faker.datatype.uuid(),
                redirectUri: faker.internet.url,
            });
            expect(authTokenResponse).toBeNull();
        });

        it('should return data', async () => {
            const modelValues = {
                name: 'test',
                clientId: faker.datatype.uuid(),
                clientSecret: faker.datatype.uuid(),
                responseType: 'code',
                redirectUri: faker.internet.url,
            }
            const oauthClientBefore = await OAuthClientModel.create(modelValues);

            const oauthClientGateway = new OAuthClientGateway();
            const response = await oauthClientGateway.validateOAuthClient({
                clientId: oauthClientBefore.clientId,
                clientSecret: oauthClientBefore.clientSecret,
                redirectUri: oauthClientBefore.redirectUri
            });
            expect(response._id).toEqual(oauthClientBefore._id);
            expect(response.clientId).toBe(oauthClientBefore.clientId);
            expect(response.clientSecret).toBe(oauthClientBefore.clientSecret);
        });
    });

    describe('getOAuthClientByName', () => {
        it('should not return data: not found', async () => {
            const oauthClientGateway = new OAuthClientGateway();

            const authTokenResponse = await oauthClientGateway.getOAuthClientByName(faker.datatype.uuid());
            expect(authTokenResponse).toBeNull();
        });

        it('should return data', async () => {
            const modelValues = {
                name:  `test_${faker.datatype.uuid()}`,
                clientId: faker.datatype.uuid(),
                clientSecret: faker.datatype.uuid(),
                responseType: 'code',
                redirectUri: faker.internet.url,
            }
            const oauthClientBefore = await OAuthClientModel.create(modelValues);

            const oauthClientGateway = new OAuthClientGateway();
            const response = await oauthClientGateway.getOAuthClientByName(modelValues.name);
            expect(response._id).toEqual(oauthClientBefore._id);
            expect(response.name).toBe(oauthClientBefore.name);
            expect(response.clientId).toBe(oauthClientBefore.clientId);
            expect(response.clientSecret).toBe(oauthClientBefore.clientSecret);
        });
    });
});