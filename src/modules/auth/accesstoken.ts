import { AccessTokenModel } from '../../db/models/accessToken.model';

import { IPersistableEntity, PersistableEntity } from '../common';

export interface IAccessToken extends IPersistableEntity {
    token: string,
    oauthClientId: KeyValueHash;
    userId: KeyValueHash;
}

export class AccessToken extends PersistableEntity implements IAccessToken {
    private _token: string;
    private _oauthClientId: KeyValueHash;
    private _userId: KeyValueHash;

    get token(): string {
        return this._token;
    }

    setToken(value): AccessToken {
        this._token = value;
        return this;
    }

    get oauthClientId(): KeyValueHash {
        return this._oauthClientId;
    }

    setOAuthClientId(value): AccessToken {
        this._oauthClientId = value;
        return this;
    }

    get userId(): KeyValueHash {
        return this._userId;
    }

    setUserId(value): AccessToken {
        this._userId = value;
        return this;
    }

    toJSON() {
        const base = super.toJSON();

        return {
            ...base,
            token: this._token,
            oauthClientId: this._oauthClientId,
            userId: this._userId,
        };
    }

    toModelValues() {
        const raw = this.toJSON();

        for (const key of Object.keys(raw)) {
            if (!raw[key]) delete raw[key];
        }
        return raw;
    }

    static fromRaw(
        raw: KeyValueHash | typeof AccessTokenModel | any,
        options?: KeyValueHash,
    ): AccessToken {
        const instance = new this();

        instance
            .setId(raw.id)
            .setName(raw.name)
            .setCreatedAt(raw.createdAt)
            .setUpdatedAt(raw.updatedAt)
            .setToken(raw.token)
            .setOAuthClientId(raw.oauthClientId)
            .setUserId(raw.userId)

        return instance;
    }
}
