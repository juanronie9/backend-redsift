import { OAuthClientModel } from './oauthClient.model';
import { AccessTokenModel } from './accessToken.model';
import { UserModel } from './user.model';

export default {
    OAuthClientModel,
    AccessTokenModel,
    UserModel,
};
