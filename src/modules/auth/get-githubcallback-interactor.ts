import { Interactor, IRequestObject } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { OAuthClientGateway } from './oauthclient-gateway';
import { AccessTokenGateway } from './accesstoken-gateway';
import { AccessToken } from './accesstoken';
import { AuthService } from './auth-service';
import { UserGateway } from '../users/user-gateway';
import { User } from '../users/user';

export interface GetGithubCallbackRequestObject extends IRequestObject {
    code: string
}

export const validationSchema = joi.object().keys({
    code: joi
        .string()
        .required()
});


export class GetGithubCallbackInteractor extends Interactor {

    private _oauthClientGateway: OAuthClientGateway = null;
    private _accessTokenGateway: AccessTokenGateway = null;
    private _userGateway: UserGateway = null;

    setOAuthClientGateway(gateway: OAuthClientGateway) {
        this._oauthClientGateway = gateway;
        return this;
    }

    setAccessTokenGateway(gateway: AccessTokenGateway) {
        this._accessTokenGateway = gateway;
        return this;
    }

    setUserGateway(gateway: UserGateway) {
        this._userGateway = gateway;
        return this;
    }

    async execute(request: GetGithubCallbackRequestObject) {
        try {

            const code = request.code;

            const oauthClient = await this._oauthClientGateway.getOAuthClientByName('github');
            if (!oauthClient) {
                return this.presenter.error([ 'OAuth client not found' ]);
            }

            const authService = new AuthService();
            const providerToken = await authService.getGithubAccessToken(oauthClient.clientId, oauthClient.clientSecret, code);
            if (!providerToken) {
                return this.presenter.error([ 'Failed to get provider access token' ]);
            }

            const providerUser = await authService.getGithubUser(providerToken);
            if (!providerUser) {
                return this.presenter.error([ 'Failed to get provider user info' ]);
            }

            let user;
            user = await this._userGateway.getUserByEmail(providerUser.email);
            if (!user) {
                // CREATE NEW USER WITH INFO AND SET OAUTH_CLIENT ID
                const userObj = new User();
                userObj.setEmail(providerUser.email);
                userObj.setName(providerUser.name);
                userObj.addOauthclient(oauthClient);
                user = await this._userGateway.createUser(userObj)
            }

            // CREATE ACCESS_TOKEN
            const accessToken = new AccessToken();
            accessToken.setOAuthClientId(oauthClient._id);
            accessToken.setUserId(user._id);
            const { token } = await this._accessTokenGateway.createAccessToken(accessToken);

            const result = {
                access_token: token,
                expires_in: 3600,
            }
            return this.presenter.success(result);
        } catch (e) {
            logger.error('Failed to get access token', request, e);
            return this.presenter.error([e.message]);
        }
    }
}
