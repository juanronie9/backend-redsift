# The Backend API

Provides a rich `backend-api` abilities.

## API

API is built on top of [OpenAPI 3.0 specification](https://swagger.io/specification/)
See: [swagger.yml](./src/templates/api-docs/swagger.yaml) for details.

#### Documentation
```
# DEV
http://localhost:3000/docs/api

# PROD
https://<domain>/docs/api
```

## Building

The project uses [TypeScript](https://github.com/Microsoft/TypeScript) as a JavaScript transpiler.
The sources are located under the `src` directory, the distributables are in the `dist`.

### Requirements:

-   Nodejs
-   MongoDB

### Environment:

```bash
APP_HTTP_PORT=3000
APP_PUBLIC_URL=http://localhost:3000
APP_ENVIRONMENT_NAME=
APP_MONGO_DATABASE=
APP_MONGO_HOST=
MONGO_PASS=
MONGO_PORT=
MONGO_SRV=
APP_MONGO_USER=
APP_ADMIN_ACCESS_IP_LIST=
```

To make the application running use

```bash
npm run build
npm run start
```


## Development

```bash
npm run debug
```

## Testing

It uses Jestjs and supertest, but it can be changed to other lib.
```bash
npm run test
```
Open one terminal with project running and other terminal to execute the e2e api tests.
```bash
npm run test:e2e
```
---

## Application Structure
- [src](./src): The main `backend` application code.
    - [api](./src/api): Defines API routers and controllers
    - [config](./src/config): Defines configuration. Examples ENV_VARS
    - [db](./src/db): MongoDB models and schemas
    - [modules](./src/modules): Defines modules and logic of the application. 
      All the modules are composed of `Interactors` used by controllers, `Gateways` to interact with DB and `Services` to handle logic or request external REST API's.   
- [test](./test): Includes all the testing of the application
- [app.ts](./app.ts): Main file. Inject packages, initiate and bootstrap application


---


# Thought Process

- Analyse the exercise
- Design the architecture
- Read OAuth and federated identity documentation
- Create the API structure
- Design database structure (MongoDB collections)
- Application development and testing
- Write Documentation

# What you found challenging
- Make quick decisions. There is very little time.
- User may have custom identity, third party federated identity or both. Solved.  

# Next Steps (improvements)

- Add real expiration token functionality
- Add refresh tokens functionality
- Develop generic solution to integrate different federated Identities using oauth
- Use Redis. Example: to cache user/auth related info
- Increase available APIs and functionalities
- Update tokens to JWT
- Improve tests, documentation 


# OAuth Grant Types used

### OAuth Resource Owner Password Flow 
For users registered in our database with username and password. Allows exchanging the usename and password for an access token.
```
curl -X POST \
  http://0.0.0.0:3000/v1/oauth/token \
  -H 'content-type: application/json' \
  -d '{
    "clientId": "CLIENT_ID",
    "clientSecret": "CLIENT_SECRET",
    "redirectUri": "REDIRECT_URI",
    "grantType": "password"
    "email": "USER_EMAIL",
    "password: "USER_PASSWORD"
}'
```
```
{
    "access_token": "e7ce0ae7-ad43-470c-8ba3-69c580219c8d",
    "expires_in": 3600
}
```

### OAuth Authorization Code Flow

The federated identity used is **Github**. 

How to integrate third party federated identities? Steps:

- The application opens a browser to send the user to the OAuth server
- The user sees the authorization prompt and approves the app’s request
- The user is redirected back to the application with an authorization code in the query string
- The application exchanges the authorization code for an access token

Note: Pre-register your oauth application in the identity provider with redirect URIs.

##### Authorization
1. Authorization Code Link: will be in the frontend button link. Example:
    ```
    http://github.com/login/oauth/authorize?client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&scope=SCOPE&state=STATE
    ```
    ```
    <a href="http://github.com/login/oauth/authorize?client_id=CLIENT_ID&redirect_uri=REDIRECT_URI&scope=SCOPE&state=STATE" class="btn btn-danger"><span class="fa fa-github"></span> Github Login</a>
    ```

2. User Authorizes Application: When the user clicks the link, they must first log in to the service to authenticate their identity (unless they are already logged in). Then they will be prompted by the service to authorize or deny the application access to their account.
   

3. Application Receives Authorization Code: If the user clicks "Allow," the service redirects the user back to your site with an authorization code.
    ```
    https://REDIRECT_URI_HERE?code=AUTH_CODE_HERE&state=1234zyx
    ```
    - code - The server returns the authorization code in the query string
    - state - The server returns the same state value that you passed
    ```
    http://0.0.0.0:3000/v1/oauth/github/callback?code=1234
    ```

#### Getting an Access Token
Exchange the authorization code for an access token by making a POST request to the authorization server's token endpoint
```
curl -X POST \
  https://github.com/login/oauth/access_token \
  -H 'content-type: application/json' \
  -H 'x-user-id: 60def766e9a13e0007e09f50' \
  -d '{
    "code": "AUTH_CODE",
    "client_id": "CLIENT_ID",
    "client_secret": "CLIENT_SECRET"
}'
```
```
{
    "access_token": "ACCESS_TOKEN",
    "expires_in": 3600
}
```
#### Get User Info
Request the API with an access_token to get userinfo
```
curl -X GET \
  https://api.github.com/user \
  -H 'content-type: application/json' \
  -H 'Authorization: token ACCESS_TOKEN_HERE'
```

### OAuth Security Considerations
- Use always Https
- Limited Access Token Lifetime
- Limited Code Authorization Lifetime
- Verification OAuth Client
- Verification RedirectUri to avoid manipulation
- reviewing the list of applications they’ve authorized to revoke access to apps they no longer use.