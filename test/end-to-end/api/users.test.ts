import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import supertest from 'supertest';
import _ from 'lodash';

const prefix = '/v1/';

function api(path) {
    return prefix + path;
}

let request;

const credentials = {
    'access-token': '',
};

describe('UsersApi', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();

        request = supertest('http://0.0.0.0:3000');

        // NOTE: Simulated data, in a real application use this flow for every test:
        // 1. Insert data in db
        // 2. Get data from db
        // 3. Verify data from db
        // 4. Clear data from db
        credentials['access-token'] = 'b3d0e953-8a14-4aec-a5b5-ed8ee021819f';
    });
    afterAll(async () => {
        // await db.close()
    });
    describe('Get GithubCallback [GET] /v1/users', () => {
        it('should fail when credentials not provided', async () => {
            const response = await request
                .get(api('users'));

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when credentials not valid', async () => {
            const badCredentials = _.clone(credentials);
            badCredentials['access-token'] = '------';
            const response = await request
                .get(api('users'))
                .set(badCredentials);

            expect(response.statusCode).toBe(401);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail: filter parameters type not valid', async () => {
            const response = await request
                .get(api('users'))
                .set(credentials)
                .query({
                    page: '------',
                    pageSize: '-----'
                })
                .set(credentials);

            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should return data', async () => {
            const response = await request
                .get(api('users'))
                .set(credentials);

            expect(response.statusCode).toBe(200);
            const { body } = response;
            expect(body.length).toBeGreaterThanOrEqual(0);
        });

        it('should return data: filter parameters', async () => {
            const response = await request
                .get(api('users'))
                .query({
                    page: 1,
                    pageSize: 10,
                    domain: 'github',
                    email: 'test@test.com'
                })
                .set(credentials);

            expect(response.statusCode).toBe(200);
            const { body } = response;
            expect(body.length).toBeGreaterThanOrEqual(1);
        });

        // ...
        // ...
    });
});
