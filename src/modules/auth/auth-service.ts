// import applicationConfig from '../../config/application';
// import { HttpStatus } from '../common';
// import { logger } from '../../app';
// import request from 'request';
import bcrypt from 'bcrypt';

import { IUserModel, UserModel } from '../../db/models/user.model';
import { AccessTokenGateway } from './accesstoken-gateway';
import { logger } from '../../app';


interface IGetGithubUserResponse {
    login: string,
    id: number,
    node_id: string,
    avatar_url: string,
    gravatar_id: string,
    url: string,
    html_url: string,
    email: string,
    name: string,
}

export class AuthService {

    constructor() {}

    async login(email: string, password: string): Promise<IUserModel> {
        if (!email || !password) {
            throw new Error('Username or password parameter missing');
        }
        email = email.trim().toLowerCase();

        const user = await UserModel.findOne({ 'email': email, password: { $exists: true } });
        if (!user) {
            throw new Error('User not found');
        }

        const isPasswordValid = bcrypt.compareSync(password, user.password);
        if (!isPasswordValid) {
            throw new Error('Password not valid');
        }
        return user;
    }

    obtainTokenFromRequest(req, headerKeys, queryKey): string {
        let token = '';
        if (req.query && req.query[queryKey]) {
            return req.query[queryKey];
        }
        if (req.headers) {
            for (const headerKey of headerKeys) {
                if (req.headers[headerKey]) {
                    token = req.headers[headerKey];
                    break;
                }
            }
        }
        return token;
    }

    async validateToken(token: string): Promise<boolean> {
        try {
            const accessTokenGateway = new AccessTokenGateway();
            const isValid = await accessTokenGateway.validateAccessToken(token);
            if (!isValid) {
                return false;
            }
            return true;
        } catch (e) {
            return false;
        }
    }

    async getGithubAccessToken(clientId: string, clientSecret: string, code: string): Promise<string> {
        const uri = `https://github.com/login/oauth/access_token`;
        const options = {
            method: 'POST',
            uri,
            body: {
                client_id: clientId,
                client_secret: clientSecret,
                code,
            },
            json: true,
        };

        const res: string = await new Promise((resolve, reject) => {
            // request(options, (err, response, body) => {
            //     if (err) {
            //         logger.warn('Error during token obtaining: ', err, body);
            //         return reject(err);
            //     }
            //
            //     if (response.statusCode >= HttpStatus.BAD_REQUEST) {
            //         reject(
            //             body ||
            //             new Error(
            //                 `Unable to obtain  the token. ErrorCode: ${response.statusCode}`,
            //             ),
            //         );
            //     }
            //
            //     return resolve(body.data.access_token);
            // });
            logger.info('MOCKED REQUEST getGithubAccessToken', options);
            const accessToken = Math.random().toString(36);
            return resolve(accessToken);
        });

        return res;
    }

    async getGithubUser(token: string) {
        const uri = `https://api.github.com/user`;
        const options = {
            method: 'GET',
            uri,
            headers: {
                Authorization: 'token ' + token,
            },
            json: true,
        };

        const res: IGetGithubUserResponse = await new Promise((resolve, reject) => {
            // request(options, (err, response, body) => {
            //     if (err) {
            //         logger.warn('Error during user obtaining: ', err, body);
            //         return reject(err);
            //     }
            //
            //     if (response.statusCode >= HttpStatus.BAD_REQUEST) {
            //         reject(
            //             body ||
            //             new Error(
            //                 `Unable to obtain  the user. ErrorCode: ${response.statusCode}`,
            //             ),
            //         );
            //     }
            //
            //     return resolve(body.data);
            // });
            logger.info('MOCKED REQUEST getGithubUser', options);
            const data: IGetGithubUserResponse = {
                login: 'defunkt',
                id: 2,
                node_id: 'MDQ6VXNlcjI=',
                avatar_url: 'https://avatars.githubusercontent.com/u/2?v=4',
                gravatar_id: '',
                url: 'https://api.github.com/users/defunkt',
                html_url: 'https://github.com/defunkt',
                email: 'defunkt@gmail.com',
                name: 'defunkt'
            }
            return resolve(data);
        });
        return res;
    }
}
