import { IUserModel, UserModel } from '../../db/models/user.model';

import { DEFAULT_PAGE_NUM, DEFAULT_PAGE_SIZE } from '../common/pagination';
import { MongoGateway } from '../common/persistance-gateway';
import { /*IUser,*/ User } from './user';
import { OAuthClientModel } from '../../db/models/oauthClient.model';

interface GetUsersParameters {
    email: string;
    domain: string;
    redirectUri: string;
    page: number;
    pageSize: number;
}

interface GetUsersResult {
    count: number;
    rows: any;
}

export class UserGateway extends MongoGateway {
    private _userModel = UserModel;

    setUserModel(model: typeof UserModel) {
        this._userModel = model;
        return this;
    }

    async getUsers({
        email,
        domain,
        redirectUri,
        page = DEFAULT_PAGE_NUM,
        pageSize = DEFAULT_PAGE_SIZE
    }: GetUsersParameters): Promise<GetUsersResult> {
        const query: any = {};

        if (email) {
            query.email = email;
        }

        if (redirectUri) {
            const oauthClientQuery: any = {};
            oauthClientQuery.redirectUri = redirectUri;
            const oauthClients = await OAuthClientModel.find(oauthClientQuery);
            query.oauthclients = { $in: oauthClients.map((oc) => oc._id) };
        }

        if (domain) {
            const oauthClientQuery: any = {};
            oauthClientQuery.name = domain;
            const oauthClients = await OAuthClientModel.find(oauthClientQuery);
            query.oauthclients = { $in: oauthClients.map((oc) => oc._id) };
        }

        const rows = await this._userModel.find(query)
            .sort({ createdAt: 'descending' })
            .skip(pageSize * (page - 1))
            .limit(pageSize);

        const count = await this._userModel.count(query);

        return {
            count,
            rows,
        };
    }

    async createUser(user: User): Promise<IUserModel> {
        const modelValues = {
            ...user.toModelValues(),
            createdAt: Date.now(),
            updatedAt: Date.now(),
        };

        const created = await this._userModel.create(modelValues);
        return created;
    }

    async getUserByEmail(email: string): Promise<IUserModel>{
        const query = {
            email,
        };
        return UserModel.findOne(query).exec();
    }
}
