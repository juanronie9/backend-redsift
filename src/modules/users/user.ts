import { UserModel } from '../../db/models/user.model';

import { IPersistableEntity, PersistableEntity } from '../common';
import { Oauthclient } from '../auth/oauthclient';
import { IOAuthClientModel, OAuthClientModel } from '../../db/models/oauthClient.model';
import bcrypt from 'bcrypt';

export interface IUser extends IPersistableEntity {
    email: string;
    password: string;
}

export class User extends PersistableEntity implements IUser {
    private _email: string;
    private _password: string;
    private readonly _oauthclients = new Set<KeyValueHash | Oauthclient | typeof OAuthClientModel>();

    get email(): string {
        return this._email;
    }

    setEmail(value): User {
        this._email = value;
        return this;
    }

    get password(): string {
        return this._password;
    }

    setPassword(value): User {
        const salt = bcrypt.genSaltSync(10);
        this._password = bcrypt.hashSync(value, salt);
        return this;
    }

    get oauthclients() {
        return [...this._oauthclients];
    }

    addOauthclient(value: Oauthclient | IOAuthClientModel | typeof OAuthClientModel): User {
        this._oauthclients.add(value);
        return this;
    }

    toJSON() {
        const base = super.toJSON();

        return {
            ...base,
            email: this._email,
            password: this._password,
            oauthclients: [this._oauthclients],
        };
    }

    toModelValues() {
        const raw = this.toJSON();

        for (const key of Object.keys(raw)) {
            if (!raw[key]) delete raw[key];
        }

        return raw;
    }

    static fromRaw(
        raw: KeyValueHash | typeof UserModel | any,
        options?: KeyValueHash,
    ): User {
        const instance = new this();

        instance
            .setId(raw.id)
            .setName(raw.name)
            .setCreatedAt(raw.createdAt)
            .setUpdatedAt(raw.updatedAt)
            .setEmail(raw.email)
            .setPassword(raw.password);
        if (raw.oauthclients) {
            (raw.oauthclients as typeof OAuthClientModel[]).forEach(value =>
                instance.addOauthclient(value),
            );
        }

        return instance;
    }
}
