import { IAccessTokenModel, AccessTokenModel } from '../../db/models/accessToken.model';
import { MongoGateway } from '../common/persistance-gateway';
import { AccessToken } from './accesstoken';
import { v4 as uuidv4 } from 'uuid';


export class AccessTokenGateway extends MongoGateway {
    private _accessTokenModel = AccessTokenModel;

    setAccessTokenModel(model: typeof AccessTokenModel) {
        this._accessTokenModel = model;
        return this;
    }

    async createAccessToken(accessToken: AccessToken): Promise<AccessToken> {
        const modelValues = {
            ...accessToken.toModelValues(),
            token: uuidv4(),
            createdAt: Date.now(),
            updatedAt: Date.now(),
        }

        const created = await this._accessTokenModel.create(modelValues);

        return AccessToken.fromRaw(created);
    }

    async validateAccessToken(token: string): Promise<IAccessTokenModel> {
        const query = {
            token,
        };

        const accessToken = await this._accessTokenModel
            .findOne(query)
            .exec();

        if (!accessToken) {
            return null;
        }

        return accessToken;
    }
}
