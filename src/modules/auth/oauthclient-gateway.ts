import { IOAuthClientModel, /*IOAuthClientModel,*/ OAuthClientModel } from '../../db/models/oauthClient.model';
import { MongoGateway } from '../common/persistance-gateway';


interface ValidateOAuthClientParameters {
    clientId: string,
    clientSecret: string,
    redirectUri: string,
}

export class OAuthClientGateway extends MongoGateway {
    private _oauthClientModel = OAuthClientModel;

    setOAuthClientModel(model: typeof OAuthClientModel) {
        this._oauthClientModel = model;
        return this;
    }

    async validateOAuthClient({ clientId, clientSecret, redirectUri }: ValidateOAuthClientParameters) {
        const query = {
            clientId,
            clientSecret,
            redirectUri
        };

        const oauthClient = await this._oauthClientModel
            .findOne(query)
            .exec();

        if (!oauthClient) {
            return null;
        }

        return oauthClient;
    }

    async getOAuthClientByName(name: string): Promise<IOAuthClientModel> {
        const query: any = {
            name
        }
        const oauthClient = await this._oauthClientModel
            .findOne(query)
            .exec();
        if (!oauthClient) {
            return null;
        }
        return oauthClient;
    }
}
