import faker from 'faker';

import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import { AuthService } from '../../../src/modules/auth/auth-service';
import { AccessToken } from '../../../src/modules/auth/accesstoken';
import { AccessTokenGateway } from '../../../src/modules/auth/accesstoken-gateway';
import { UserGateway } from '../../../src/modules/users/user-gateway';
import { User } from '../../../src/modules/users/user';

describe('AuthService', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();
    })

    afterAll(async () => {
        // await db.close()
    })

    describe('login', () => {
        it('should fail: missing parameters', async () => {
            try {
                const authService = new AuthService();
                await authService.login('test@test.test', '');
            } catch (e) {
                expect(e.message).toBe('Username or password parameter missing');
            }
        });
        it('should fail: user not found', async () => {
            try {
                const authService = new AuthService();
                await authService.login(faker.internet.email(), 'password');
            } catch (e) {
                expect(e.message).toBe('User not found');
            }
        });
        it('should fail: user password not valid', async () => {
            const email = `test_${faker.internet.email().toLowerCase()}`;
            const name = `test_${faker.datatype.uuid().toLowerCase()}`;
            const password = 'Asdf1234';

            const userGateway = new UserGateway();
            const user = new User();
            user.setEmail(email);
            user.setName(name);
            user.setPassword(password);
            await userGateway.createUser(user);
            try {
                const authService = new AuthService();
                await authService.login(email, 'password-not-valid');
            } catch (e) {
                expect(e.message).toBe('Password not valid');
            }
        });
        it('should return data', async () => {
            const email = `test_${faker.internet.email().toLowerCase()}`;
            const name = `test_${faker.datatype.uuid().toLowerCase()}`;
            const password = 'Asdf1234';

            const userGateway = new UserGateway();
            const user = new User();
            user.setEmail(email);
            user.setName(name);
            user.setPassword(password);
            const responseUser = await userGateway.createUser(user);

            expect(responseUser.email).toBe(email);
            expect(responseUser.name).toBe(name);

            const authService = new AuthService();
            const response = await authService.login(email, password);
            expect(response.email).toBe(email);
        });
    });

    describe('validateToken', () => {
        it('should fail: token not valid', async () => {
            const authService = new AuthService();
            const isValid = await authService.validateToken('');
            expect(isValid).toBe(false);
        });
        it('should return data', async () => {
            const accessToken = new AccessToken();
            const accessTokenGateway = new AccessTokenGateway();
            const response = await accessTokenGateway.createAccessToken(accessToken);

            const authService = new AuthService();
            const isValid = await authService.validateToken(response.token);
            expect(isValid).toBe(true);
        });
    });

    describe('getGithubAccessToken', () => {
        it('should return data', async () => {
            const authService = new AuthService();
            const isValid = await authService.getGithubAccessToken(
                '',
                '',
                '',
            );
            expect(isValid).not.toBeNull()
        });
    });

    describe('getGithubUser', () => {
        it('should return data', async () => {
            const authService = new AuthService();
            const user = await authService.getGithubUser(faker.datatype.uuid());
            expect(user.email).toBe('defunkt@gmail.com');
            expect(user.name).toBe('defunkt');
        });
    });
});