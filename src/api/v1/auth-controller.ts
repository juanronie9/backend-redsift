import { ExpressController, ExpressJSONShowPresenter } from '../../modules/core';
import { OAuthClientGateway } from '../../modules/auth/oauthclient-gateway';
import { AccessTokenGateway } from '../../modules/auth/accesstoken-gateway';
import { GetAccesstokenInteractor,
    GetAccessTokenRequestObject,
    validationSchema as createTokensSchema
} from '../../modules/auth/get-accesstoken-interactor';
import {
    GetGithubCallbackInteractor,
    GetGithubCallbackRequestObject,
    validationSchema as getGithubCallbackSchema,
} from '../../modules/auth/get-githubcallback-interactor';
import { UserGateway } from '../../modules/users/user-gateway';

export class AuthController extends ExpressController {
    constructor() {
        super();

        this.router.post('/token', this.validator.validateBody(createTokensSchema), this.getAccessToken.bind(this));
        this.router.get('/github/callback', this.validator.validateQuery(getGithubCallbackSchema), this.getGithubCallback.bind(this));
    }

    async getAccessToken(req, res, next) {
        const oauthClientGateway = new OAuthClientGateway();
        const accessTokenGateway = new AccessTokenGateway();

        const interactor = new GetAccesstokenInteractor();

        const presenter = new ExpressJSONShowPresenter(req, res, next);

        const request = req.body as GetAccessTokenRequestObject;

        interactor.setPresenter(presenter)
            .setAccessTokenGateway(accessTokenGateway)
            .setOAuthClientGateway(oauthClientGateway);

        await interactor.execute(request);
    }

    async getGithubCallback(req, res, next) {
        const oauthClientGateway = new OAuthClientGateway();
        const accessTokenGateway = new AccessTokenGateway();
        const userGateway = new UserGateway();

        const interactor = new GetGithubCallbackInteractor();

        const presenter = new ExpressJSONShowPresenter(req, res, next);

        const request = req.query as GetGithubCallbackRequestObject;

        interactor.setPresenter(presenter)
            .setAccessTokenGateway(accessTokenGateway)
            .setOAuthClientGateway(oauthClientGateway)
            .setUserGateway(userGateway)

        await interactor.execute(request);
    }
}
