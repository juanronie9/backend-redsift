import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import supertest from 'supertest';
import _ from 'lodash';
import { AccessTokenModel, IAccessTokenModel } from '../../../src/db/models/accessToken.model';
import { IUserModel, UserModel } from '../../../src/db/models/user.model';

const prefix = '/v1/';

function api(path) {
    return prefix + path;
}

let request;

const bodyRequest = {
    clientId: '2_3nio9v1agigw844kssgk4kswksgs0wk4okkoksg4080osoos84',
    clientSecret: '4394ff69idq8g08k4wso4ck8008w0g44ck88wo8c8cw80wk8c4',
    grantType: 'password',
    email: 'test@test.com',
    password: 'Asdf1234'
};

// NOTE: Simulated data, in a real application use this flow for every test:
// 1. Insert data in db
// 2. Get data from db
// 3. Verify data from db
// 4. Clear data from db

describe('AuthApiTest', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();

        request = supertest('http://0.0.0.0:3000');
    });
    afterAll(async () => {
        // await db.close()
    });
    describe('Get token [POST] /v1/oauth/token', () => {
        it('should fail when body is not provided', async () => {
            const response = await request
                .post(api('oauth/token'))
                .send({});

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters required missing', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            delete bodyRequestTest.clientId;
            const response = await request
                .post(api('oauth/token'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters type not valid', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.clientId = 1234;
            const response = await request
                .post(api('oauth/token'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when data is not valid: oauth client', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.clientId = '-----'
            const response = await request
                .post(api('oauth/token'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(404);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should success', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            const response = await request
                .post(api('oauth/token'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(200);
            const { body } = response;
            expect(body.access_token).not.toBeNull();

            // -----------------
            // Verification data
            // -----------------

            // 1. Verify received access_token exist
            const accessToken: IAccessTokenModel = await AccessTokenModel.findOne({ token: body.access_token }).exec();
            expect(accessToken).toBeDefined();
            expect(accessToken.userId).not.toBeNull();
            expect(accessToken.oauthClientId).not.toBeNull();

            // 2. Verify user exists and oauthclient
            const user: IUserModel = await UserModel.findById(accessToken.userId);
            expect(user.oauthclients.length).toBeGreaterThanOrEqual(1);
            expect(user.email).not.toBeNull();
            expect(user.name).not.toBeNull();
        });
    });

    describe('Get GithubCallback [GET] /v1/oauth/github/callback', () => {
        it('should fail when query parameters is not provided', async () => {
            const response = await request
                .get(api('oauth/github/callback'));

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should success', async () => {
            const response = await request
                .get(api('oauth/github/callback'))
                .query({
                    code: '1234'
                });

            expect(response.statusCode).toBe(200);
            const { body } = response;
            expect(body.access_token).not.toBeNull();

            // -----------------
            // Verification data
            // -----------------

            // 1. Verify received access_token exist
            const accessToken: IAccessTokenModel = await AccessTokenModel.findOne({ token: body.access_token }).exec();
            expect(accessToken).toBeDefined();
            expect(accessToken.userId).not.toBeNull();
            expect(accessToken.oauthClientId).not.toBeNull();

            // 2. Verify user exists and oauthclient
            const user: IUserModel = await UserModel.findById(accessToken.userId);
            expect(user.oauthclients.length).toBeGreaterThanOrEqual(1);
            expect(user.email).not.toBeNull();
            expect(user.name).not.toBeNull();
        });
    });

    // ...
    // ...
});
