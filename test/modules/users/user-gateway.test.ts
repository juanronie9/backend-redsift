import faker from 'faker';
import { MongoConnector } from '../../../src/modules/common/mongo-connector';
import applicationConfig from '../../../src/config/application';
import { UserGateway } from '../../../src/modules/users/user-gateway';
import { User } from '../../../src/modules/users/user';


describe('UserGateway', () => {
    beforeAll(async () => {
        const connector = new MongoConnector();
        connector.setConfig(applicationConfig.mongoDb);
        await connector.connect();
    });
    afterAll(async () => {
        // await db.close()
    });

    describe('getUsers', () => {
        it('should return data empty: not found', async () => {
            const userGateway = new UserGateway();

            const usersResponse = await userGateway.getUsers({
                email: faker.internet.email(),
                redirectUri: faker.internet.url(),
                domain: 'github',
                page: 1,
                pageSize: 10
            });
            expect(usersResponse.count).toBe(0);
            expect(usersResponse.rows.length).toBe(0);
        });
    });

    describe('getUserByEmail', () => {
        it('should not return data: not found', async () => {
            const userGateway = new UserGateway();
            const usersResponse = await userGateway.getUserByEmail(faker.internet.email());
            expect(usersResponse).toBeNull();
        });
        it('should return data', async () => {
            const email = faker.internet.email();
            const name = 'test';
            const password = 'Asdf1234';

            const userGateway = new UserGateway();
            const user = new User();
            user.setEmail(email);
            user.setName(name);
            user.setPassword(password);
            const responseUser = await userGateway.createUser(user);

            expect(responseUser.email).toBe(email);
            expect(responseUser.name).toBe(name);

            const usersResponse = await userGateway.getUserByEmail(email);
            expect(usersResponse._id).toBeDefined();
            expect(usersResponse.email).toBe(email);
        });
    });
});